
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:30PM

See merge request itentialopensource/adapters/adapter-calix_cms!12

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-calix_cms!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:43PM

See merge request itentialopensource/adapters/adapter-calix_cms!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:56PM

See merge request itentialopensource/adapters/adapter-calix_cms!8

---

## 0.4.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!7

---

## 0.3.3 [03-28-2024]

* Changes made at 2024.03.28_13:40PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:57PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:26AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!4

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!3

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!2

---

## 0.1.3 [07-12-2021] & 0.1.2 [07-09-2021]

- changes for passing nodename per call, also added getDeviceConfig call testing on POC

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix!1

---

## 0.1.1 [07-01-2021]

- Initial Commit

See commit 0916e61

---
