# Calix Management System

Vendor: Calix
Homepage: https://www.calix.com/

Product: Calix Management System
Product Page: https://www.calix.com/products/platform/intelligent-access/software/calix-management-system.html

## Introduction
We classify Calix Management System into the Data Center and Network Services domais as Calix Management System provides the capability to get information about and make changes to infrastructure.

"A rich set of tools for network and service configuration, surveillance, performance and administration, delivering full Fault, Configuration, Accounting, Performance and Security (FCAPS) for the complete suite of Calix Unified Access network devices"

## Why Integrate
The Calix Management System adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Calix Management System. With this adapter you have the ability to perform operations such as:

- Get Device Config
- Provision Device

## Additional Product Documentation
The [Calix API Documentation](https://documenter.getpostman.com/view/3367549/S1TN7M75)