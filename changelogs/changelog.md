
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix_cms!2

---

## 0.1.3 [07-12-2021] & 0.1.2 [07-09-2021]

- changes for passing nodename per call, also added getDeviceConfig call testing on POC

See merge request itentialopensource/adapters/controller-orchestrator/adapter-calix!1

---

## 0.1.1 [07-01-2021]

- Initial Commit

See commit 0916e61

---
